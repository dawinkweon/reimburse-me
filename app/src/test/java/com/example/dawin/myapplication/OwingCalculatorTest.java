package com.example.dawin.myapplication;

import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by dawinkorionhealth.com on 28/04/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class OwingCalculatorTest {
    private static final long user0 = 0, user1 = 1, user2 = 2, user3 = 3;
    private List<Owing> owingList;
    private OwingCalculator calculator;

    @Before
    public void setup() {
        this.owingList = new ArrayList<>();
    }

    /*
     *  Grocery 1, Payer: User0
     *  Ower: User1
     *  Amount $200, Split amount $100
     *
     *  Grocery 2, Payer: User1
     *  Ower: User0
     *  Amount $100, Split amount $50
     *
     *  Result:
     *  User1 owes User0 $50
     */
    @Test
    public void testCalculator() {
        this.owingList.clear();
        this.owingList.add(createOwing(user0, user1, 200.00));
        this.owingList.add(createOwing(user1, user0, 100.00));


        Set<OwingSummary> outputSummaryList = OwingCalculator.generateSummary(this.owingList);

        Assert.assertEquals(1, outputSummaryList.size());
        OwingSummary output = (OwingSummary)(outputSummaryList.toArray()[0]);

        Assert.assertEquals(user1, output.getOwer().getId());
        Assert.assertEquals(user0, output.getPayer().getId());
        Assert.assertEquals(100.0, output.getAmount());
    }

    private Owing createOwing(long payerId, long owerId, double amount) {
        Owing owing = new Owing();
        owing.setPayingUser(createUser(payerId));
        owing.setGroceryName("Grocery: " + System.currentTimeMillis());
        owing.setAmount(amount);
        owing.setOwingUser(createUser(owerId));
        return owing;
    }

    private User createUser(long userId) {
        User user = new User();
        user.setId(userId);
        user.setName("User: " + userId);
        return user;
    }
}
