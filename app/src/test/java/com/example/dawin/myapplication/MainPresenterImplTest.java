package com.example.dawin.myapplication;

import com.example.dawin.myapplication.Helpers.GroceryHelper;
import com.example.dawin.myapplication.Helpers.UserHelper;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.Presenter.HomePresenter;
import com.example.dawin.myapplication.Presenter.HomePresenterImpl;
import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.internal.RealmCore;
import io.realm.log.RealmLog;

import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created by dawinkorionhealth.com on 25/03/18.
 */
@RunWith(JUnit4.class)
public class MainPresenterImplTest {
    @Mock
    Realm realm;

    @Mock
    UserHelper userHelper;

    @Mock
    GroceryHelper groceryHelper;

    @Mock
    RealmQuery<User> userRealmQuery;

    @Mock
    RealmResults<User> userRealmResults;

    private ExpectedException thrown = ExpectedException.none();

    private HomePresenterImpl presenter;
    @Before
    public void setUp() {
        // Setup Realm to be mocked. The order of these matters
        mockStatic(RealmCore.class);
        mockStatic(RealmLog.class);
        mockStatic(Realm.class);
        mockStatic(RealmConfiguration.class);
        Realm.init(RuntimeEnvironment.application);

        MockitoAnnotations.initMocks(this);
        this.presenter = new HomePresenterImpl();
        this.presenter.setUserHelper(this.userHelper);
        this.presenter.setGroceryHelper(this.groceryHelper);

        Mockito.when(this.realm.where(User.class)).thenReturn(this.userRealmQuery);
        Mockito.when(this.userRealmQuery.findAll()).thenReturn(this.userRealmResults);
    }

    // useless test
    @Test
    public void getAllUsers() {
        Mockito.when(this.realm.copyFromRealm(Mockito.any(Iterable.class)))
                .thenReturn(new ArrayList<>());
        this.presenter.loadUsers();

        Mockito.verify(this.realm).copyFromRealm(Mockito.any(Iterable.class));
    }

    @Test
    public void addGroceryDelegatesToHelper() throws Exception{
        this.presenter.addGrocery("new grocery",
                0,
                 createDefaultPayingUsersList(),
                200.0);

        Mockito.verify(this.groceryHelper).createGrocery("new grocery",
                0,
                 createDefaultPayingUsersList(),
                200.0);
    }

    @Test
    public void cannotAddGroceryWhenUserNotFound() {
        this.presenter.addGrocery("new grocery",
                0,
                 createDefaultPayingUsersList(),
                200);
        // failure message is shown on view
    }

    /**
     * Cannot add grocery when list of owers are empty
     * There needs to be at least 1 payer and 1 ower
     * TODO What if you want to just log your expenses without needing reimbusements?
     */
    @Test
    public void cannotAddGroceryWhenNoPayers() {
        this.presenter.addGrocery("new grocery",
                0,
                 ImmutableList.of(),
                200);
    }

    /**
     * On AddGrocery success, should show a message on view
     */
    @Test
    public void addGroceryShowsSuccessMessage() {
        //TODO
//        Mockito.verify(this.viewCallBack).onAddGrocerySuccess("<success message here");
    }

    @Test
    public void failingToAddGroceryShowsFailureMessage() throws Exception{
//        this.thrown.expect(UserNotFoundException.class);
//
//        Mockito.doThrow(new UserNotFoundException("user not found"))
//                .when(this.groceryHelper)
//                .createGrocery(Mockito.anyString(),
//                                Mockito.anyLong(),
//                                Mockito.any(List.class),
//                                Mockito.anyDouble());

        this.presenter.addGrocery("new grocery",
                -1,
                 createDefaultPayingUsersList(),
                200.0);
    }

    @Test
    public void unableToAddGroceryWhenUsingInvalidTotalCost() {
        this.presenter.addGrocery("new grocery",
                0,
                createDefaultPayingUsersList(),
                -999.99);
//        Mockito.verify(this.viewCallBack).onAddGroceryFailure("Total cost must be more than $0");
        Mockito.verifyNoMoreInteractions(this.groceryHelper);
    }

    @Test
    public void addUserDelegatesToHelper() {
        this.presenter.setupMockUser("new username");

        Mockito.verify(this.userHelper).createUser("new username");
    }

    private List<Long> createDefaultPayingUsersList() {
        return ImmutableList.of(new Long(1),
                new Long(2),
                new Long(3));
    }



}
