package com.example.dawin.myapplication;

import com.example.dawin.myapplication.Helpers.UserHelper;
import com.example.dawin.myapplication.Helpers.UserHelperImpl;
import com.example.dawin.myapplication.Models.User;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.internal.RealmCore;
import io.realm.log.RealmLog;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by dawinkorionhealth.com on 13/03/18.
 */
@RunWith(PowerMockRunner.class)
@Config(constants = BuildConfig.class, sdk = 19)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@SuppressStaticInitializationFor("io.realm.internal.Util")
@PrepareForTest({Realm.class, RealmConfiguration.class, RealmQuery.class, RealmResults.class, RealmCore.class, RealmLog.class})
public class UserHelperImplTest {
    private static final long MAX_USER_ID = 10;

    // Paying user that paid for the grocery
    @Mock
    User user0;

    // Owing users that owe the payer
    @Mock
    User newUser;

    @Mock
    Realm realm;

    @Mock
    RealmQuery<User> userRealmQuery;

    @Mock
    Number maxUserId;

    @Mock
    RealmResults<User> userRealmResults;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    // Actual Impl
    private UserHelperImpl helper;

    @Before
    public void setup(){
        // Setup Realm to be mocked. The order of these matters
        mockStatic(RealmCore.class);
        mockStatic(RealmLog.class);
        mockStatic(Realm.class);
        mockStatic(RealmConfiguration.class);
        Realm.init(RuntimeEnvironment.application);

        this.realm = PowerMockito.mock(Realm.class);
        PowerMockito.when(Realm.getDefaultInstance()).thenReturn(this.realm);

        this.helper = new UserHelperImpl();

        PowerMockito.when(this.realm.where(User.class)).thenReturn(this.userRealmQuery);
        Mockito.when(this.userRealmQuery.findAll()).thenReturn(this.userRealmResults);
        Mockito.when(this.userRealmQuery.max(User.ID)).thenReturn(this.maxUserId);
        Mockito.when(this.maxUserId.longValue()).thenReturn(MAX_USER_ID);

        PowerMockito.when(this.realm.createObject(Mockito.eq(User.class), Mockito.anyLong()))
                .thenReturn(newUser);
    }

    // Realm instance is able to be used
    @Test
    public void getDefaultRealm(){
        assertNotNull(this.realm);
        assertFalse(this.realm.isClosed());
    }

    @Test
    public void getAllUsers(){
        this.helper.getAllUsers();
        Mockito.verify(this.userRealmQuery).findAll();
    }

    @Test
    public void successfullyGetUniqueId() {

        long uniqueId = this.helper.getUniqueUserId();
        assertEquals(MAX_USER_ID + 1, uniqueId);
    }


    @Test
    public void createUserGetsCorrectID() {
        long newUserId = this.helper.createUser("new user");

        Mockito.verify(this.realm).createObject(User.class, MAX_USER_ID + 1);
        // user's id should be an increment of the max id in realm
        assertEquals(MAX_USER_ID + 1, newUserId);
    }

    @Test
    public void createUserWhenNoUserExists() {
        Mockito.when(this.userRealmQuery.max(User.ID)).thenReturn(null);

        long newId = this.helper.createUser("new user");
        assertEquals(0, newId);
    }

    @Test
    public void successfullyCreateUser() {
        this.helper.createUser("new user");

        Mockito.verify(this.realm).createObject(Mockito.eq(User.class), Mockito.anyLong());
        Mockito.verify(this.newUser).setName("new user");
    }
}
