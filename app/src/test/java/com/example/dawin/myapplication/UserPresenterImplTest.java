package com.example.dawin.myapplication;

import com.example.dawin.myapplication.Helpers.OwingHelper;
import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.Presenter.UserPresenter;
import com.example.dawin.myapplication.Presenter.UserPresenterImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.internal.RealmCore;
import io.realm.log.RealmLog;

import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */
@RunWith(PowerMockRunner.class)
@Config(constants = BuildConfig.class, sdk = 19)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@SuppressStaticInitializationFor("io.realm.internal.Util")
@PrepareForTest({Realm.class, RealmConfiguration.class, RealmQuery.class, RealmResults.class, RealmCore.class, RealmLog.class})

public class UserPresenterImplTest {

    @Mock
    OwingHelper owingHelper;

    @Mock
    UserPresenter.CallBack callBack;

    @Mock
    Realm realm;

    private UserPresenterImpl presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        // Setup Realm to be mocked. The order of these matters
        mockStatic(RealmCore.class);
        mockStatic(RealmLog.class);
        mockStatic(Realm.class);
        mockStatic(RealmConfiguration.class);
        Realm.init(RuntimeEnvironment.application);

        this.realm = PowerMockito.mock(Realm.class);
        PowerMockito.when(Realm.getDefaultInstance()).thenReturn(this.realm);

        this.presenter = new UserPresenterImpl(callBack);
        this.presenter.setOwingHelper(this.owingHelper);

        // create fake owing list UNPAID
        List<Owing> fakeOwings = new ArrayList<>();
        fakeOwings.add(createOwing(0, 1, 10, "chocolate", Owing.UNPAID));
        fakeOwings.add(createOwing(0, 1, 10, "pizza", Owing.UNPAID));
        fakeOwings.add(createOwing(0, 2, 10, "dogs", Owing.UNPAID));
        fakeOwings.add(createOwing(0, 2, 10, "popcorn", Owing.UNPAID));
        fakeOwings.add(createOwing(0, 3, 10, "something", Owing.UNPAID));
        Mockito.when(this.owingHelper.getUnpaidOwings(Mockito.anyInt())).thenReturn(fakeOwings);

        fakeOwings = new ArrayList<>();
        fakeOwings.add(createOwing(0, 1, 10, "paid0", Owing.PAID));
        fakeOwings.add(createOwing(0, 1, 10, "paid1", Owing.PAID));
        fakeOwings.add(createOwing(0, 2, 10, "paid2", Owing.PAID));
        fakeOwings.add(createOwing(0, 2, 10, "paid3", Owing.PAID));
        fakeOwings.add(createOwing(0, 3, 10, "paid4", Owing.PAID));
        Mockito.when(this.owingHelper.getPaidOwings(Mockito.anyInt())).thenReturn(fakeOwings);
    }

    @Test
    public void onUserSelectedDisplaysUserOwings() {
        this.presenter.onUserSelected(0);
//        Mockito.verify(this.callBack).displayOwings("");
        Mockito.verify(this.owingHelper).getPaidOwings(0);
        Mockito.verify(this.owingHelper).getUnpaidOwings(0);
        Mockito.verify(this.callBack).displayOwings(Mockito.anyString());
    }

    @Test
    public void onBtnPayClick() {
        List<Owing> fakeOwings = new ArrayList<>();
        fakeOwings.add(createOwing(0, 1, 10, "chocolate", Owing.UNPAID));

        this.presenter.onBtnPayClick();

        Mockito.verify(this.owingHelper).pay(Mockito.anyList(), 1);
        Mockito.verify(this.callBack).displayOwings(Mockito.anyString());
    }

    private Owing createOwing(long owingUserId,
                              long payingUserId,
                              double amount,
                              String groceryName,
                              int status) {
        Owing owing = new Owing();
        owing.setGroceryName(groceryName);
        owing.setOwingUser(createUser("n" + owingUserId, owingUserId));
        owing.setOwingUserId(owingUserId);
        owing.setPayingUser(createUser("n" + payingUserId, payingUserId));
        owing.setAmount(amount);
        owing.setStatus(status);
        return owing;
    }

    private User createUser(String name, long id) {
        User user = new User();
        user.setName(name);
        user.setId(id);
        return user;
    }
}
