package com.example.dawin.myapplication;

import com.example.dawin.myapplication.Helpers.GroceryHelper;
import com.example.dawin.myapplication.Helpers.GroceryHelperImpl;
import com.example.dawin.myapplication.Helpers.UserHelper;
import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;
import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.internal.RealmCore;
import io.realm.log.RealmLog;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by dawinkorionhealth.com on 13/03/18.
 */
@RunWith(PowerMockRunner.class)
@Config(constants = BuildConfig.class, sdk = 19)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@SuppressStaticInitializationFor("io.realm.internal.Util")
@PrepareForTest({Realm.class, RealmConfiguration.class, RealmQuery.class, RealmResults.class, RealmCore.class, RealmLog.class})
public class GroceryHelperImplTest {

    public static final String GROCERY_NAME = "new grocery";
    // Paying user that paid for the grocery
    @Mock
    User user0;

    @Mock
    User user1;

    @Mock
    User user2;

    @Mock
    User user3;

    @Mock
    User userThatDoesNotExist;

    @Mock
    Realm realm;

    @Mock
    UserHelper userHelper;

    @Mock
    Grocery grocery;

    @Mock
    RealmQuery<User> userRealmQuery;
    @Mock
    RealmResults<User> userRealmResults;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    // Actual Impl
    private GroceryHelperImpl helper;

    @Before
    public void setup() throws Exception {
        // Setup Realm to be mocked. The order of these matters
        mockStatic(RealmCore.class);
        mockStatic(RealmLog.class);
        mockStatic(Realm.class);
        mockStatic(RealmConfiguration.class);
        Realm.init(RuntimeEnvironment.application);

        // set up mock users
        setUpUser(this.user0, 0);
        setUpUser(this.user1, 1);
        setUpUser(this.user2, 2);
        setUpUser(this.user3, 3);
        setUpUser(this.userThatDoesNotExist, 404);

        // set up user helper
        Mockito.when(this.userHelper.getUserObject(this.userThatDoesNotExist.getId()))
                .thenThrow(new Exception("User: 404 not found"));

        this.realm = PowerMockito.mock(Realm.class);
        this.helper = new GroceryHelperImpl();
        this.helper.setUserHelper(this.userHelper);
    }

    // Realm instance is able to be used
    @Test
    public void getDefaultRealm() {
        assertNotNull(this.realm);
        assertFalse(this.realm.isClosed());
    }

    /**
     * Successfully add grocery with default owing people
     */
    @Test
    public void createGroceryRetrievesPayingUser() throws Exception {

        this.helper.createGrocery(GROCERY_NAME, 0, ImmutableList.of(), 200);

        Mockito.verify(this.userHelper).getUserObject(0);
    }

    /**
     * Successfully add correct payment using single ower
     */
    @Test
    public void createGroceryWithSingleOwer() throws Exception {
        double expectedAmountPerPerson = 200.0 / 2;

        Owing expectedDuePayment = new Owing();
        expectedDuePayment.setAmount(expectedAmountPerPerson);

        this.helper.createGrocery(GROCERY_NAME,
                0,
                ImmutableList.of(new Long(1)), 200.0);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<Grocery> groceryCaptor = ArgumentCaptor.forClass(Grocery.class);

//        Mockito.verify(this.userHelper).copyUserToRealm(userCaptor.capture());
//        assertEquals(0, userCaptor.getValue().getId());
//
//        Mockito.verify(this.user0).addGrocery(groceryCaptor.capture());
//        groceryCaptor.getValue().getOwings()
//                .stream()
//                .forEach(o -> assertEquals(expectedAmountPerPerson, o.getAmount()));
    }

    /**
     * Successfully add correct payments using multiple owers
     * All owing instances should point to the same payer and split amount
     */
    @Test
    public void createGroceryWithMultipleOwers() throws Exception {
        double expectedAmountPerPerson = 200.0 / 4;
        List<Long> expectedOwers = ImmutableList.of(new Long(1),
                new Long(2),
                new Long(3));

        this.helper.createGrocery(GROCERY_NAME,
                0,
                expectedOwers, 200.0);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<Grocery> groceryCaptor = ArgumentCaptor.forClass(Grocery.class);

//        Mockito.verify(this.user0).addGrocery(groceryCaptor.capture());
//        groceryCaptor.getValue().getOwings()
//                .stream()
//                .forEach(o -> assertEquals(expectedAmountPerPerson, o.getAmount()));
//
//        Mockito.verify(this.userHelper).copyUserToRealm(userCaptor.capture());
//        assertEquals(0, userCaptor.getValue().getId());

    }

    /**
     * Cannot create grocery when owing user does not exist in realm
     */
    @Test
    public void cannotCreateGroceryWhenUserNotFound() throws Exception {
//        this.thrown.expect(UserNotFoundException.class);

        // create users that cannot be found from realm
        List<Long> expectedOwers = ImmutableList.of(this.userThatDoesNotExist.getId());

        this.helper.createGrocery(GROCERY_NAME,
                this.user0.getId(),
                expectedOwers,
                200.0);
    }

    private void setUpUser(User user, long id) throws Exception {
        Mockito.when(this.userHelper.getUserObject(id))
                .thenReturn(user);
        Mockito.when(user.getId()).thenReturn(id);

        RealmList<Grocery> groceries = new RealmList<>();
        Mockito.when(user.getGroceries()).thenReturn(groceries);
    }
}
