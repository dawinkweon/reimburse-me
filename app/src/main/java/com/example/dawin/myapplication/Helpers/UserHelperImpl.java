package com.example.dawin.myapplication.Helpers;

import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dawinkorionhealth.com on 18/03/18.
 */

public class UserHelperImpl implements UserHelper {

    private Realm realm;

    public UserHelperImpl(){
        this.realm = Realm.getDefaultInstance();
    }


    @Override
    public RealmResults<User> getAllUsers(){
        return this.realm.where(User.class).findAll();
    }

    @Override
    public long createUser(String userName) {

        long uniqueUserId = getUniqueUserId();

        this.realm.beginTransaction();
        User newUser = realm.createObject(User.class, uniqueUserId);
        newUser.setName(userName);
        this.realm.commitTransaction();

        return uniqueUserId;
    }

    @Override
    public User getUserObject(long userId) throws UserNotFoundException{
        User owingUser = getAllUsers().where().equalTo(User.ID, userId).findFirst();
        if(owingUser == null){
            throw new UserNotFoundException("User with id: " + userId + " was not found");
        }
        return owingUser;
    }

    public long getUniqueUserId() {
        Number highestId = this.realm
                .where(User.class)
                .max(User.ID);
        if(highestId == null){
            return 0;
        }
        return highestId.longValue() + 1;
    }

    @Override
    public List<User> findAllUsers() {
        RealmResults<User> allUsers = this.realm.where(User.class).findAll();
        return this.realm.copyFromRealm(allUsers);
    }

}
class UserNotFoundException extends Exception {
    public UserNotFoundException(String message){
        super(message);
    }
}

