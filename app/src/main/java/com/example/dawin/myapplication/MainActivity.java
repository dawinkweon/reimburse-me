package com.example.dawin.myapplication;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.dawin.myapplication.Fragments.HomeFragment;
import com.example.dawin.myapplication.Fragments.UserFragment;

import io.realm.Realm;

/**
 * Created by Dawin on 3/4/2018.
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MyApplication.TAG;

    private DrawerLayout drawerLayout;

    private Realm realm;

    private Fragment homeFragment;
    private Fragment usersFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.drawerLayout = findViewById(R.id.drawer_layout);

        setupFragments();
        setupToolbar();
    }

    private void setupToolbar() {
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setTitle("Reimburse Me");
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    private void setupFragments() {
        this.homeFragment = new HomeFragment();
        this.usersFragment = new UserFragment();


        replaceFragment(getSupportFragmentManager(), homeFragment);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((menuItem) -> {
            // set item as selected to persist highlight
            menuItem.setChecked(true);
            // close drawer when item is tapped
            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {
                case R.id.home:
                    replaceFragment(getSupportFragmentManager(), homeFragment);
                    break;
                case R.id.users:
                    replaceFragment(getSupportFragmentManager(), usersFragment);
                    break;
            }
            return true;
        });
    }

    private static void replaceFragment(FragmentManager fragmentManager, Fragment fragment) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
