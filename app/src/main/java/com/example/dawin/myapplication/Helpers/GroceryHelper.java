package com.example.dawin.myapplication.Helpers;

import com.example.dawin.myapplication.Models.Grocery;

import java.util.List;

/**
 * Provides a CRUD interface for Grocery
 * Created by Dawin on 3/4/2018.
 */

public interface GroceryHelper {

    // creates a new grocery item
    void createGrocery(String groceryName, long payingUserId, List<Long> owingUserIds, double totalCost) throws UserNotFoundException;

    List<Grocery> findAllGroceries();
}
