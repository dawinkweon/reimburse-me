package com.example.dawin.myapplication.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.R;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dawinkorionhealth.com on 6/03/18.
 */

public class CheckableUserAdapter extends ArrayAdapter<User> {

    // activity context
    private Context mContext;

    // list of users fetched from realm
    private List<CheckableUser> spinnerItems;

    private CheckableUserAdapter.CheckBoxListener checkBoxListener;

    private CheckableUser lastDisabledItem;

    public CheckableUserAdapter(Context context, CheckableUserAdapter.CheckBoxListener checkBoxListener, int resource, List<User> fetchedUsers) {
        super(context, resource, fetchedUsers);

        this.mContext = context;
        this.checkBoxListener = checkBoxListener;
        this.spinnerItems = new ArrayList<>();
        for(int i = 0; i < fetchedUsers.size(); ++i){
            this.spinnerItems.add(new CheckableUser(fetchedUsers.get(i), i));
        }
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int _position, View convertView,
                              ViewGroup parent) {
        int position = _position - 1;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.user_item_with_checkbox, null);
            holder = new ViewHolder();
            holder.mTextView = convertView.findViewById(R.id.text);
            holder.mCheckBox = convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(position == -1) {
            // display first item as 'select user'
            holder.mTextView.setText("Select User");
            holder.mCheckBox.setVisibility(View.INVISIBLE);
            return convertView;
        }
        holder.mTextView.setText(this.spinnerItems.get(position).getName());

        // To check weather checked event fire from getview() or user input
        copySettings(holder.mCheckBox, this.spinnerItems.get(position));

        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spinnerItems.get(position).setSelected(isChecked);
                holder.mCheckBox.setChecked(isChecked);

                checkBoxListener.notifyCheckChanged();
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }

    private void copySettings(CheckBox checkbox, CheckableUser item){
        checkbox.setEnabled(item.isEnabled());
        checkbox.setSelected(item.isSelected());
        checkbox.setTag(item.getPosition());
    }

    public List<Long> getSelectedUsers() {
        return this.spinnerItems.stream()
                .filter(item -> item.isSelected())
                .map(item -> item.getId())
                .collect(Collectors.toList());
    }

    public void disableUser(long userId) {
        resetUserStates();

        // disable user
        CheckableUser item = this.spinnerItems.stream()
                .filter(i -> i.getId() == userId)
                .findFirst()
                .get();
        if(item != null) {
            item.setEnabled(false);
        } else {
            // shouldn't reach here
            // log errors or handle it
        }
    }

    private void resetUserStates() {
        for(CheckableUser user : this.spinnerItems) {
            user.setDefaultState();
        }
    }

    @Override
    public int getCount() {
        return super.getCount() + 1;
    }

    public interface CheckBoxListener{
        void notifyCheckChanged();
    }
}