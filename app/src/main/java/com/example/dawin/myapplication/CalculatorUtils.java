package com.example.dawin.myapplication;

/**
 * Created by dawinkorionhealth.com on 22/03/18.
 */

public interface CalculatorUtils {
    public static double calculateSplitAmount(int numberOfOwers, double totalCost){
            return totalCost / (numberOfOwers + 1);
    }
}
