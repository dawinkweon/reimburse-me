package com.example.dawin.myapplication.Helpers;

import com.example.dawin.myapplication.CalculatorUtils;
import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.RealmUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Initial point of contact to create and add a new grocery
 * Created by Dawin on 3/4/2018.
 */

public class GroceryHelperImpl implements GroceryHelper {
    private Realm realm;
    private UserHelper userHelper;

    public GroceryHelperImpl() {
        this.realm = Realm.getDefaultInstance();
        this.userHelper = new UserHelperImpl();
    }

    public void setUserHelper(UserHelper userHelper) {
        this.userHelper = userHelper;
    }

    @Override
    public void createGrocery(String groceryName, long payingUserId, List<Long> owingUserIds, double totalCost) throws UserNotFoundException {
        this.realm.beginTransaction();

        User payingUser = this.userHelper.getUserObject(payingUserId);
        List<User> owingUsers = new ArrayList<>();
        for(long id : owingUserIds) {
            owingUsers.add(this.userHelper.getUserObject(id));
        }
        int numberOfPayees = owingUserIds.size();

        assert numberOfPayees > 0;
        assert totalCost > 0.0;

        createGrocery(groceryName,
                payingUser,
                totalCost,
                numberOfPayees,
                RealmUtils.convertToRealmList(owingUsers));

        double splitAmount = CalculatorUtils.calculateSplitAmount(owingUserIds.size(), totalCost);

        Owing owingPayment;
        for (User user : owingUsers) {
            createOwingPayment(groceryName, user, splitAmount, payingUser);
        }
        //TODO need to update tests
        // this is the only time the realm is called
        this.realm.commitTransaction();
    }

    private void createOwingPayment(String groceryName,
                                    User user,
                                    double splitAmount,
                                    User payingUser) {
        Owing owing = this.realm.createObject(Owing.class, UUID.randomUUID().toString());
        owing.setStatus(Owing.UNPAID);
        owing.setGroceryName(groceryName);
        owing.setOwingUser(user);
        owing.setOwingUserId(user.getId());
        owing.setAmount(splitAmount);
        owing.setPayingUser(payingUser);
    }

    private void createGrocery(String groceryName,
                               User payingUser,
                               double totalCost,
                               int numberOfPayees,
                               RealmList owingUsers) {

        String id = UUID.randomUUID().toString();
        Grocery grocery = this.realm.createObject(Grocery.class, id);
        grocery.setName(groceryName);
        grocery.setPayer(payingUser);
        grocery.setCost(totalCost);
        grocery.setNumberOfPayees(numberOfPayees);
        grocery.setOwingUsers(owingUsers);
    }

    @Override
    public List<Grocery> findAllGroceries() {
        RealmResults<Grocery> allGroceries = this.realm.where(Grocery.class).findAll();
        return this.realm.copyFromRealm(allGroceries);
    }
}
