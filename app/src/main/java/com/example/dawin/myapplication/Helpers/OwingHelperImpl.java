package com.example.dawin.myapplication.Helpers;

import com.example.dawin.myapplication.Models.Owing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */

public class OwingHelperImpl implements OwingHelper {
    private Realm realm;

    public OwingHelperImpl() {
        this.realm = Realm.getDefaultInstance();
    }

    public OwingHelperImpl(Realm realm) { this.realm = realm; }

    @Override
    public List<Owing> getAllOwings(long userId) {
        RealmResults<Owing> results = this.realm.where(Owing.class)
                .equalTo("owingUserId", userId)
                .sort("status")
                .findAll();

        return this.realm.copyFromRealm(results);
    }

    @Override
    public List<Owing> getAllOwings(List<Long> selectedUsers) {
        List<Owing> output = new ArrayList<>();

        for (long user : selectedUsers) {
            List<Owing> retrieved = getAllOwings(user);
            output.addAll(retrieved);
        }
        return output;
    }

    @Override
    public void pay(List<Owing> owingList) {
        this.realm.beginTransaction();
        RealmResults<Owing> owingRealmList = this.realm.where(Owing.class).findAll();

        for(Owing owing : owingList) {
            Owing retrieved = owingRealmList.where().equalTo(Owing.ID, owing.getId()).findFirst();
            // do some error handling here
            retrieved.setStatus(Owing.PAID);
        }
        this.realm.commitTransaction();
    }
}
