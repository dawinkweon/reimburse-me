package com.example.dawin.myapplication.Models;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dawinkorionhealth.com on 8/03/18.
 */

public class Grocery extends RealmObject {

    @PrimaryKey
    private String id;

    private String name;

    private double cost;

    private User payer;

    private int numberOfPayees;

    private RealmList<User> owingUsers;

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public User getPayer() {
        return payer;
    }

    public void setPayer(User payer) {
        this.payer = payer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public int getNumberOfPayees() {
        return numberOfPayees;
    }

    public void setNumberOfPayees(int numberOfPayees) {
        this.numberOfPayees = numberOfPayees;
    }

    public RealmList<User> getOwingUsers() {
        return owingUsers;
    }

    public void setOwingUsers(RealmList<User> owingUsers) {
        this.owingUsers = owingUsers;
    }
}
