package com.example.dawin.myapplication;

import android.app.Application;
import android.util.Log;

import com.example.dawin.myapplication.Helpers.UserHelper;
import com.example.dawin.myapplication.Helpers.UserHelperImpl;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */

public class MyApplication extends Application {

    public static final String TAG = "Reimburse";

    public MyApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "MyApplication is starting...");
        configureRealm();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    private void configureRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("productionv1")
                .schemaVersion(1)
                .build();

        Realm realm = Realm.getInstance(config);
        Realm.setDefaultConfiguration(config);

        // Temporarily add users (Dawin, Yong, Minh, Lite)
        UserHelper userHelper = new UserHelperImpl();
        if (userHelper.getAllUsers().size() == 0) {
            userHelper.createUser("Dawin");
            userHelper.createUser("Yong");
            userHelper.createUser("Lite");
            userHelper.createUser("Minh");
            userHelper.createUser("Other");
        }
    }
}
