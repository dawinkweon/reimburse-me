package com.example.dawin.myapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.dawin.myapplication.CalculatorUtils;
import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.MyApplication;
import com.example.dawin.myapplication.Presenter.HomePresenterImpl;
import com.example.dawin.myapplication.R;
import com.example.dawin.myapplication.UI.CheckableUser;
import com.example.dawin.myapplication.UI.CheckableUserAdapter;
import com.example.dawin.myapplication.UI.GroceryAdapter;
import com.example.dawin.myapplication.UI.UserAdapter;

import java.util.List;

import io.realm.Realm;

/**
 * Created by dawinkorionhealth.com on 3/04/18.
 */

public class HomeFragment extends Fragment implements CheckableUserAdapter.CheckBoxListener{

    private static final String TAG = MyApplication.TAG;

    private Spinner spinnerUsers, spinnerPayingUsers;
    private EditText textGroceryName, textTotalCost, textCostPerPerson;
    private Button btnAddGroceryAction;

    private RecyclerView groceryView;
    private RecyclerView.Adapter groceryAdapter;
    private RecyclerView.LayoutManager layoutManager;

    // adapter and views for spinner that selects payees
    private CheckableUserAdapter checkableUserAdapter;
    private UserAdapter userAdapter;

    private HomePresenterImpl presenter;
    private Realm realm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //temporarily add users each time
        this.realm = Realm.getDefaultInstance();
        this.presenter = new HomePresenterImpl();

        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        this.spinnerUsers = getView().findViewById(R.id.spinnerPayers);

        this.spinnerPayingUsers = getView().findViewById(R.id.spinnerPayingUsers);

        this.textGroceryName = getView().findViewById(R.id.textGroceryName);

        this.textTotalCost = getView().findViewById(R.id.textTotalCost);
        this.textTotalCost.addTextChangedListener(getOnTextChangedListener());

        this.textCostPerPerson = getView().findViewById(R.id.textCostPerPerson);

        this.btnAddGroceryAction = getView().findViewById(R.id.btnAdd);
        this.btnAddGroceryAction.setOnClickListener(getOnAddClickListener());

        this.groceryView = getView().findViewById(R.id.grocery_view);
        this.groceryView.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(getContext());
        this.groceryView.setLayoutManager(this.layoutManager);

        loadSpinnerItems();
        loadGroceryItems();

    }

    private TextWatcher getOnTextChangedListener () {
        return new TextWatcher () {
            @Override
            public void beforeTextChanged (CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                updateCostPerPerson();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private View.OnClickListener getOnAddClickListener () {
        return (f) -> {
            if(!isValidInputs()) return;

            // get selected grocery arguments from UI
            User selectedPayingUser = (User)this.spinnerUsers.getSelectedItem();
            List<Long> selectedUsers = checkableUserAdapter.getSelectedUsers();

            String groceryName = this.textGroceryName.getText().toString();

            double totalCost;
            String strTotalCost = this.textTotalCost.getText().toString();
            totalCost = Double.parseDouble(strTotalCost);

            // delegate to presenter
            presenter.addGrocery(groceryName,
                    selectedPayingUser.getId(),
                    selectedUsers,
                    totalCost);
            loadGroceryItems();
        };
    }

    private void updateCostPerPerson () {
        double totalCost;

        // validate total cost
        String strTotalCost = textTotalCost.getText().toString();
        if (strTotalCost.isEmpty()) {
            textCostPerPerson.setText("");
            return;
        }

        int numberOfPayers;
        // validate number of payers
        numberOfPayers = checkableUserAdapter.getSelectedUsers().size();
        if (numberOfPayers < 1) {
            textCostPerPerson.setText("");
            return;
        }

        totalCost = Double.parseDouble(strTotalCost);

        double costPerPerson = CalculatorUtils.calculateSplitAmount(numberOfPayers, totalCost);
        textCostPerPerson.setText(Double.toString(costPerPerson));
    }

    private void loadSpinnerItems() {
        List<User> allUsers = this.presenter.loadUsers();

        this.userAdapter = new UserAdapter(getContext(), android.R.layout.simple_list_item_1, allUsers);
        this.spinnerUsers.setAdapter(userAdapter);
        this.spinnerUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                User selectedUser = (User)adapterView.getSelectedItem();
                checkableUserAdapter.disableUser(selectedUser.getId());
                // refresh the adapter
                spinnerPayingUsers.setAdapter(checkableUserAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkableUserAdapter = new CheckableUserAdapter(getContext(), this, 0, allUsers);
        this.spinnerPayingUsers.setAdapter(checkableUserAdapter);
    }

    private void loadGroceryItems() {
        List<Grocery> groceries = this.presenter.loadGroceries();

        this.groceryAdapter = new GroceryAdapter(getContext(), groceries);
        this.groceryView.setAdapter(this.groceryAdapter);

    }

    @Override
    public void notifyCheckChanged() {
        updateCostPerPerson();
    }

    private boolean isValidInputs() {
        if(this.textTotalCost.getText().toString().isEmpty()) {
            this.textTotalCost.setError("Total cost cannot be empty");
            return false;
        }
        if(this.textGroceryName.getText().toString().isEmpty()) {
            this.textGroceryName.setError("Grocery name cannot be empty");
            return false;
        }
        // if no paying users were selected, invoke an error on selected text view
        TextView errorText = this.spinnerPayingUsers.getSelectedView()
                .findViewById(R.id.text);
        if(checkableUserAdapter.getSelectedUsers().size() <= 0) {
            errorText.setError("Must select at least one payee");
            return false;
        }

        errorText.setError(null);
        return true;
    }
}
