package com.example.dawin.myapplication.Models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dawin on 3/5/2018.
 */

public class User extends RealmObject{

    public static final String ID = "id";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    @PrimaryKey
    private long id;

    private RealmList<Grocery> groceries;

    public long getId() {
        return this.id;
    }
    public void setId(long id){
        this.id = id;
    }

    public RealmList<Grocery> getGroceries() {
        return this.groceries;
    }

    public void addGrocery (Grocery grocery) {
        this.groceries.add(grocery);
    }

    @Override
    public int hashCode() {
        return (int)this.id;
    }

    @Override
    public boolean equals(Object target) {
        if (target instanceof User) {
            User uTarget = (User)target;
            return this.id == uTarget.getId();
        }
        return false;
    }
}
