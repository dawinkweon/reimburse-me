package com.example.dawin.myapplication;

import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */

public class OwingListProcessor {
    private List<Owing> userOwings;

    public OwingListProcessor(List<Owing> userOwings) {
        this.userOwings = userOwings;
    }

    public Map<User, Double> processForSingleUser() {
        Map<User, Double> reduced = new HashMap<>();
        for(Owing owing : this.userOwings) {
            User payingUser = owing.getPayingUser();
            double current = reduced.getOrDefault(payingUser, 0.0);
            reduced.put(payingUser,current + owing.getAmount());
        }
        return reduced;
    }

    public Set<OwingSummary> processForMultipleUsers() {
        Set<OwingSummary> summaryList = new HashSet<>();

        Map<User, Map<User, Double>> map = new HashMap<>();

        for(Owing owing : this.userOwings) {
            User payer = owing.getPayingUser();

            Map<User, Double> innerMap = map.get(payer);
            if(innerMap == null) {
                innerMap = new HashMap<>();
            }
            double currentAmount = innerMap.getOrDefault(owing.getOwingUser(), 0.0);
            innerMap.put(owing.getOwingUser(), currentAmount + owing.getAmount());

            map.put(payer, innerMap);
        }

        for(User user : map.keySet()) {
            Map<User, Double> innerMap = map.get(user);

            for(User target : innerMap.keySet()) {

                double left, right;

                left = innerMap.getOrDefault(target, 0.0);

                Map<User, Double> retrievedMap = map.get(target);
                if(retrievedMap != null) {
                    right = retrievedMap.getOrDefault(user, 0.0);
                } else {
                    right = 0.0;
                }
                if(left == right) {
                    continue;
                }
                double diff = Math.abs(left - right);
                // determine who the ower is

                OwingSummary newSummary;
                if(left > right) {
                    newSummary = new OwingSummary(user, target, diff);
                } else {
                    newSummary = new OwingSummary(target, user, diff);
                }
                summaryList.add(newSummary);
            }
        }

        return summaryList;
    }
}
