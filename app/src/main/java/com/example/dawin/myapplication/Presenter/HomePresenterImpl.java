package com.example.dawin.myapplication.Presenter;

import android.util.Log;

import com.example.dawin.myapplication.Helpers.GroceryHelper;
import com.example.dawin.myapplication.Helpers.GroceryHelperImpl;
import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.MyApplication;
import com.example.dawin.myapplication.Helpers.UserHelper;
import com.example.dawin.myapplication.Helpers.UserHelperImpl;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by dawinkorionhealth.com on 6/03/18.
 */

public class HomePresenterImpl implements HomePresenter {
    private static final String TAG = MyApplication.TAG;

    private GroceryHelper groceryHelper;
    private UserHelper userHelper;

    public HomePresenterImpl() {
        this.groceryHelper = new GroceryHelperImpl();
        this.userHelper = new UserHelperImpl();
    }

    public void setGroceryHelper(GroceryHelper groceryHelper) {
        this.groceryHelper = groceryHelper;
    }

    public void setUserHelper(UserHelper userHelper) {
        this.userHelper = userHelper;
    }

    @Override
    public List<User> loadUsers() {
        return this.userHelper.findAllUsers();
    }

    @Override
    public void addGrocery(String groceryName, final long payerId,
                           final List<Long> owingUserIds,
                           final double totalCost) {
        if(totalCost <= 0) {
            Log.d(TAG, "Total cost must be more than $0");
            return;
        }
        try{
            this.groceryHelper.createGrocery(groceryName, payerId, owingUserIds , totalCost);
        }catch(Exception e){
            Log.d(TAG, e.toString());
        }
        Log.d(TAG, "Added grocery: " + groceryName);
    }

    @Override
    public List<Grocery> loadGroceries() {
        Log.d(TAG, "Groceries loaded to view");
        return this.groceryHelper.findAllGroceries();
    }
}
