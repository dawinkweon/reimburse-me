package com.example.dawin.myapplication.Presenter;

import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.User;

import java.util.List;

/**
 * Created by dawinkorionhealth.com on 6/03/18.
 */

public interface HomePresenter {

    List<Grocery> loadGroceries();

    List<User> loadUsers();

    void addGrocery(String groceryName, final long payerId,
                    final List<Long> owingUserIds,
                    final double totalCost);


}
