package com.example.dawin.myapplication;

import java.util.List;

import io.realm.RealmList;

/**
 * Created by dawinkorionhealth.com on 5/05/18.
 */

public class RealmUtils {

    public static <T> RealmList<T> convertToRealmList(List<T> list) {
        RealmList<T> realmList = new RealmList<T>();
        list.stream().forEach(el -> realmList.add(el));
        return realmList;
    }
}
