package com.example.dawin.myapplication;

import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;

/**
 * Created by dawinkorionhealth.com on 28/04/18.
 */

public class OwingSummary {

    private final User payer;
    private final User ower;
    private final double amount;

    public OwingSummary(final User payer, final User ower, final double amount) {
        this.payer = payer;
        this.ower = ower;
        this.amount = amount;
    }

    public User getPayer() { return this.payer; }

    public User getOwer() { return this.ower; }

    public double getAmount() { return this.amount; }

    @Override
    public String toString() {
        return String.format("%s owes %s $%.2f", payer.getName(), ower.getName(), amount);
    }

    @Override
    public int hashCode() {
        int result = 1;
        final int prime = 31;
        result = prime * this.payer.hashCode() + this.payer.hashCode();
        result = prime * this.ower.hashCode() + this.ower.hashCode();
        result = prime * (int)this.amount + (int)this.amount;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof OwingSummary) {
            OwingSummary oObj = (OwingSummary)obj;
            return this.payer.equals(oObj.getPayer())
                    && this.ower.equals(oObj.getOwer());
        }
        return false;
    }
}
