package com.example.dawin.myapplication.UI;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawinkorionhealth.com on 31/03/18.
 */

public class GroceryAdapter extends RecyclerView.Adapter<GroceryAdapter.ViewHolder> {
    private Context activityContext;
    private List<Grocery> groceries;

    public GroceryAdapter(Context activityContext, List<Grocery> groceries) {
        this.activityContext = activityContext;
        this.groceries = groceries;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView name;
        public TableLayout table;

        public ViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.name);
            this.table = view.findViewById(R.id.table);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grocery_row, parent, false);
         return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Grocery grocery = this.groceries.get(position);

        String bindedText = String.format("User '%s' bought '%s' costing %.2f\nCosts are shared with [",
                grocery.getPayer().getName(),
                grocery.getName(),
                grocery.getCost());
        String owingUsersText = "";
        for(User user : grocery.getOwingUsers()) {
            owingUsersText += user.getName() + ",";
        }
        owingUsersText = owingUsersText.substring(0, owingUsersText.length() - 1);
        owingUsersText += "]";
        bindedText += owingUsersText;

        holder.name.setText(bindedText);
    }

    @Override
    public int getItemCount() {
        return this.groceries.size();
    }
}
