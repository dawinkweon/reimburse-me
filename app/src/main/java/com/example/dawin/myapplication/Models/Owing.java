package com.example.dawin.myapplication.Models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dawinkorionhealth.com on 21/03/18.
 */

public class Owing extends RealmObject{

    public static final String ID = "id";
    public static final int UNPAID = 0;
    public static final int PAID = 1;

    @PrimaryKey
    private String id;

    private User owingUser;

    private User payingUser;

    private long owingUserId;

    private double amount;

    private String groceryName;

    private int status;

    public String getId() {
        return id;
    }

    public long getOwingUserId() {
        return owingUserId;
    }

    public void setOwingUserId(long owingUserId) {
        this.owingUserId = owingUserId;
    }

    public User getPayingUser() { return this.payingUser; }

    public User getOwingUser() {
        return this.owingUser;
    }

    public void setPayingUser(User payingUser) { this.payingUser = payingUser; }

    public void setOwingUser(User setOwingUser) {
        this.owingUser = setOwingUser;
    }
    
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setGroceryName(String groceryName) {
        this.groceryName = groceryName;
    }

    public String getGroceryName() {
        return groceryName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
