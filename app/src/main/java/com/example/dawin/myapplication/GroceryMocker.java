package com.example.dawin.myapplication;

import android.util.Log;

import com.example.dawin.myapplication.Helpers.GroceryHelper;
import com.example.dawin.myapplication.Helpers.GroceryHelperImpl;
import com.example.dawin.myapplication.Helpers.UserHelper;
import com.example.dawin.myapplication.Helpers.UserHelperImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dawinkorionhealth.com on 5/05/18.
 */

public class GroceryMocker {
    private static final String TAG = "GroceryMocker";

    private static final String[] mockUsers = new String[]{"Dawin", "Yong", "Minh", "Lite"};

    private UserHelperImpl userHelper;
    private GroceryHelperImpl groceryHelper;

    public GroceryMocker() {
        this.userHelper = new UserHelperImpl();
        this.groceryHelper = new GroceryHelperImpl();
    }


    public void createMocks() throws Exception {
        for(String user : mockUsers) {
            setupMockUser(user);
        }
        setupMockGroceries();
    }

    private void setupMockUser(final String userName) {
        long id = this.userHelper.createUser(userName);
        Log.d(TAG, "Created user: " + userName);
    }

    private void setupMockGroceries() throws Exception {
        List<Long> owers = new ArrayList<>();
        owers.add(new Long(1));
        owers.add(new Long(2));
        owers.add(new Long(3));

        this.groceryHelper.createGrocery("detergent",
                0,
                owers,
                200.00);

        this.groceryHelper.createGrocery("chicken breasts",
                0,
                owers,
                200.00);

        this.groceryHelper.createGrocery("shower gel",
                0,
                owers,
                200.00);

        this.groceryHelper.createGrocery("rice noodles",
                0,
                owers,
                200.00);

        this.groceryHelper.createGrocery("pasta",
                0,
                owers,
                200.00);
        Log.d(TAG, "5 Mock groceries added");
    }
}
