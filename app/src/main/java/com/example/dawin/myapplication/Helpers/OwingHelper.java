package com.example.dawin.myapplication.Helpers;

import com.example.dawin.myapplication.Models.Owing;

import java.util.List;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */

public interface OwingHelper {

    List<Owing> getAllOwings(long userId);

    List<Owing> getAllOwings(List<Long> selectedUsers);

    void pay(List<Owing> owingList);
}
