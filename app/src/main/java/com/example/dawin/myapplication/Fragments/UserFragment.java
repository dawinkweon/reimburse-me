package com.example.dawin.myapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.OwingSummary;
import com.example.dawin.myapplication.Presenter.UserPresenter;
import com.example.dawin.myapplication.Presenter.UserPresenterImpl;
import com.example.dawin.myapplication.R;
import com.example.dawin.myapplication.UI.CheckableUserAdapter;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */

public class UserFragment extends android.support.v4.app.Fragment implements UserPresenter.CallBack, CheckableUserAdapter.CheckBoxListener {

    private Spinner spinnerUsers;
    private TextView textOwing;
    private TextView textReducedOwing;
    private Button btnPay;

    private CheckableUserAdapter checkableUserAdapter;

    private UserPresenterImpl presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.presenter = new UserPresenterImpl(this);
        return inflater.inflate(R.layout.user_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.spinnerUsers = view.findViewById(R.id.spinnerUsers);

        this.textOwing = view.findViewById(R.id.textOwing);
        this.textOwing.setMovementMethod(new ScrollingMovementMethod());

        this.textReducedOwing = view.findViewById(R.id.textReducedOwing);
        this.textOwing.setMovementMethod(new ScrollingMovementMethod());

        this.btnPay = view.findViewById(R.id.btnPay);
        this.btnPay.setOnClickListener((v) -> {
            this.presenter.onBtnPayClick();
        });
        loadUsers();
    }

    private void loadUsers() {

        List<User> allUsers = this.presenter.loadUsers();

        checkableUserAdapter = new CheckableUserAdapter(getContext(), this, 0, allUsers);
        this.spinnerUsers.setAdapter(checkableUserAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void displayOwings(List<Owing> owingsList) {
        String txtOutput = "";
        for(Owing owing : owingsList) {
            String row = String.format("'%s' owes $%.2f to '%s' for grocery '%s'",
                    owing.getOwingUser().getName(),
                    owing.getAmount(),
                    owing.getPayingUser().getName(),
                    owing.getGroceryName());
            row += owing.getStatus() == Owing.PAID ? " [PAID]" : " [UNPAID]";
            txtOutput += row + "\n";
        }
        this.textOwing.setText(txtOutput);
    }

    @Override
    public void displayProcessedOwings(Map<User, Double> reducedOwing, String owingUser) {
        String outputText = "";
        for(User user : reducedOwing.keySet()) {
            String row = String.format("'%s' owes $%.2f to '%s'",
                    owingUser,
                    reducedOwing.get(user).doubleValue(),
                    user.getName());
            outputText += row + "\n";
        }
        this.textReducedOwing.setText(outputText);
    }

    @Override
    public void displayProcessedOwings(Set<OwingSummary> processed) {
        String outputText = "Summary:\n\n";
        OwingSummary[] list = processed.toArray(new OwingSummary[0]);
        for(OwingSummary summary : list) {
            String row = String.format("'%s' owes $%.2f to '%s'",
                    summary.getOwer().getName(),
                    summary.getAmount(),
                    summary.getPayer().getName());
            outputText += row + "\n";
        }
        this.textReducedOwing.setText(outputText);
    }

    @Override
    public void notifyCheckChanged() {
        List<Long> selectedUsers = checkableUserAdapter.getSelectedUsers();
        if(selectedUsers.size() == 1) {
            this.presenter.onUserSelected(selectedUsers.get(0));
        } else {
            this.presenter.onMultipleUsersSelected(selectedUsers);
        }
    }
}
