package com.example.dawin.myapplication.Presenter;

import com.example.dawin.myapplication.Helpers.OwingHelperImpl;
import com.example.dawin.myapplication.Helpers.UserHelper;
import com.example.dawin.myapplication.Helpers.UserHelperImpl;
import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.MyApplication;
import com.example.dawin.myapplication.Helpers.OwingHelper;
import com.example.dawin.myapplication.OwingListProcessor;
import com.example.dawin.myapplication.OwingSummary;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */

public class UserPresenterImpl implements UserPresenter {
    private static final String TAG = MyApplication.TAG;
    private CallBack callback;

    private OwingHelper owingHelper;
    private UserHelper userHelper;

    private List<Owing> cachedUnpaidOwings;
    private long selectedUserId;
    private List<Long> selectedUsers;

    private boolean isMultiSelect;

    public UserPresenterImpl(CallBack callBack) {
        this.userHelper = new UserHelperImpl();
        this.owingHelper = new OwingHelperImpl();
        this.callback = callBack;
    }

    @Override
    public List<User> loadUsers() {
        return this.userHelper.findAllUsers();
    }

    @Override
    public void onUserSelected(long userId) {
        this.isMultiSelect = false;
        this.selectedUserId = userId;

        User selectedUser = null;
        try{
            selectedUser = this.userHelper.getUserObject(userId);
        } catch (Exception e) {
            // TODO should receive the user object as an input not the id
            // ignore the error for now
        }

        // display owings that the selected user has
        List<Owing> owings = this.owingHelper.getAllOwings(userId);
        this.callback.displayOwings(owings);

        List<Owing> unpaidOwings = owings.stream()
                .filter(owing -> owing.getStatus() == Owing.UNPAID)
                .collect(Collectors.toList());
        this.cachedUnpaidOwings = unpaidOwings;

        // process the list of owings and display how much is owed to each user
        OwingListProcessor owingListProcessor = new OwingListProcessor(unpaidOwings);
        Map<User, Double> reducedOwing = owingListProcessor.processForSingleUser();
        this.callback.displayProcessedOwings(reducedOwing, selectedUser.getName());
    }

    @Override
    public void onMultipleUsersSelected(List<Long> selectedUsers) {
        this.isMultiSelect = true;
        this.selectedUsers = selectedUsers;

        // display owings of all the selected users
        List<Owing> owings = this.owingHelper.getAllOwings(selectedUsers);
        this.callback.displayOwings(owings);

        List<Owing> unpaidOwings = owings.stream()
                .filter(owing -> owing.getStatus() == Owing.UNPAID)
                .collect(Collectors.toList());
        this.cachedUnpaidOwings = unpaidOwings;

        // process the list and display the summary for multiple users
        OwingListProcessor owingListProcessor = new OwingListProcessor(unpaidOwings);
        Set<OwingSummary> processed = owingListProcessor.processForMultipleUsers();
        this.callback.displayProcessedOwings(processed);
    }

    @Override
    public void onBtnPayClick() {
        // pay for the unpaid owings
        if(this.cachedUnpaidOwings != null) {
            this.owingHelper.pay(this.cachedUnpaidOwings);
        }

        // refresh the view to display updated owings
        if (!this.isMultiSelect) {
            onUserSelected(this.selectedUserId);
        } else {
            onMultipleUsersSelected(this.selectedUsers);
        }
    }

    public void setOwingHelper(OwingHelper owingHelper) {
        this.owingHelper = owingHelper;
    }

    public void setUserHelper(UserHelper userHelper) {
        this.userHelper = userHelper;
    }
}
