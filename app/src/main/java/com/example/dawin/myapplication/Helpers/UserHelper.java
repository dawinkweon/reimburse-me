package com.example.dawin.myapplication.Helpers;

import com.example.dawin.myapplication.Models.Grocery;
import com.example.dawin.myapplication.Models.User;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dawinkorionhealth.com on 18/03/18.
 */

public interface UserHelper {
    RealmResults<User> getAllUsers();

    User getUserObject(long userId) throws UserNotFoundException;

    long createUser(String userName);

    long getUniqueUserId();

    List<User> findAllUsers();
}
