package com.example.dawin.myapplication.UI;

import com.example.dawin.myapplication.Models.User;

/**
 * Created by dawinkorionhealth.com on 6/03/18.
 */

public class CheckableUser {

    private User user;

    private boolean isSelected;

    private boolean isEnabled;

    private int position;

    public CheckableUser(User user, int position){
        this.user = user;
        setDefaultState();
        this.position = position;
    }

    public String getName(){
        return this.user.getName();
    }

    public long getId(){
        return this.user.getId();
    }

    public boolean isSelected(){
        return this.isSelected;
    }

    public void setSelected(boolean selected){
        this.isSelected = selected;
    }

    public void setEnabled(boolean enabled){
        this.isEnabled = enabled;
    }

    public void setDefaultState() {
        this.setSelected(false);
        this.setEnabled(true);
    }

    public boolean isEnabled(){
        return this.isEnabled;
    }

    public int getPosition() {
        return position;
    }
}

