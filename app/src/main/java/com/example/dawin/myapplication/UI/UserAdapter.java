package com.example.dawin.myapplication.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.R;

import java.util.List;

/**
 * Created by dawinkorionhealth.com on 6/03/18.
 */

public class UserAdapter extends ArrayAdapter<User> {

    // activity context
    private Context mContext;

    // list of users fetched from realm
    private List<User> users;

    public UserAdapter(Context context, int resource, List<User> users) {
        super(context, resource, users);

        this.mContext = context;
        this.users = users;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.user_item, null);
            holder = new ViewHolder();
            holder.mTextView = convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(this.users.get(position).getName());
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
    }


}