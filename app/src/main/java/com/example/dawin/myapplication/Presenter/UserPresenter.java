package com.example.dawin.myapplication.Presenter;

import com.example.dawin.myapplication.Models.Owing;
import com.example.dawin.myapplication.Models.User;
import com.example.dawin.myapplication.OwingSummary;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by dawinkorionhealth.com on 11/04/18.
 */

public interface UserPresenter {

    List<User> loadUsers();

    void onUserSelected(long userId);

    void onBtnPayClick();

    void onMultipleUsersSelected(List<Long> selectedUsers);

    interface CallBack {
        void displayOwings(List<Owing> owingList);

        void displayProcessedOwings(Map<User, Double> reducedOwing, String owingUser);

        void displayProcessedOwings(Set<OwingSummary> processed);
    }
}
